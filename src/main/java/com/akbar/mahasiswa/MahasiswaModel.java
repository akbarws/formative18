package com.akbar.mahasiswa;

import org.springframework.stereotype.Component;

@Component
public class MahasiswaModel {

	private String nik, name, age;
	
	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	
}
