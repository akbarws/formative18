package com.akbar.mahasiswa;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MahasiswaController {

	@Autowired
	MahasiswaInterface mahasiswaInterface;

	public boolean addMahasiswa(String nik, String name, String age) {
		if(isValidNik(nik) && isValidName(name) && isValidAge(age)) {
			ArrayList<MahasiswaModel> listMahasiswa = mahasiswaInterface.retrieveAllData();
			MahasiswaModel newMahasiswa = new MahasiswaModel();
			newMahasiswa.setNik(nik);
			newMahasiswa.setName(name);
			newMahasiswa.setAge(age);
			listMahasiswa.add(newMahasiswa);			
			return true;
		} else {
			return false;
		}
	}
	
	public boolean deleteMahasiswa(String nik) {
		ArrayList<MahasiswaModel> listMahasiswa = mahasiswaInterface.retrieveAllData();
		int indexMahasiswa = isNikExist(listMahasiswa, nik); 
		if(indexMahasiswa==-1)
			return false;
		else {
			listMahasiswa.remove(indexMahasiswa);
			return true;
		}
	}

	int isNikExist(ArrayList<MahasiswaModel> listMahasiswa, String nik) {
		int index = -1;
		for(int i=0; i<listMahasiswa.size(); i++)
			if(listMahasiswa.get(i).getNik().equals(nik))
				index = i;
		return index;
	}

	boolean isValidNik(String nik) {
		String regex = "^NIK-\\d+";
		return Pattern.matches(regex, nik);
	}

	boolean isValidName(String name) {
		String regex = "[a-zA-Z]+";
		return Pattern.matches(regex, name);
	}
	
	boolean isValidAge(String age) {
		String regex = "\\d+";
		return Pattern.matches(regex, age) && Integer.valueOf(age) > 17;
	}
}
