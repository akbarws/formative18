package com.akbar.mahasiswa;

import java.util.ArrayList;

public interface MahasiswaInterface {
	ArrayList<MahasiswaModel> retrieveAllData();
}
