package com.akbar.mahasiswa;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)

class MahasiswaTest {

	@Mock
	MahasiswaInterface mahasiswaInterfaceMock;
	
	@InjectMocks
	MahasiswaController mahasiswaController;
	
	@Test
	void testAddMahasiswa() {
		String nik = "NIK-124", name = "Akbar", age = "20";
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();
		listMahasiswaMock.add(addMahasiswa(nik, name, age));
		
		lenient().when(mahasiswaInterfaceMock.retrieveAllData()).thenReturn(listMahasiswaMock);
		mahasiswaController.addMahasiswa(nik, name, age);
		assertEquals(2, listMahasiswaMock.size());
	}

	@Test
	void testAddMahasiswaFailName() {
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();		
		lenient().when(mahasiswaInterfaceMock.retrieveAllData()).thenReturn(listMahasiswaMock);
		String nik = "NIK-124", name = "Akbar123", age = "20";
		assertFalse(mahasiswaController.addMahasiswa(nik, name, age));
	}
	
	@Test
	void testAddMahasiswaFailNIK() {
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();		
		lenient().when(mahasiswaInterfaceMock.retrieveAllData()).thenReturn(listMahasiswaMock);
		String nik = "NIK-aaa", name = "Akbar", age = "20";
		assertFalse(mahasiswaController.addMahasiswa(nik, name, age));
	}
	
	@Test
	void testAddMahasiswaFailAge() {
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();		
		lenient().when(mahasiswaInterfaceMock.retrieveAllData()).thenReturn(listMahasiswaMock);
		String nik = "NIK-124", name = "Akbar", age = "15";
		assertFalse(mahasiswaController.addMahasiswa(nik, name, age));
	}

	@Test
	void testDeleteMahasiswa() {
		String nik = "NIK-124", name = "Akbar2", age = "20";
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();
		listMahasiswaMock.add(addMahasiswa(nik, name, age));
		
		lenient().when(mahasiswaInterfaceMock.retrieveAllData()).thenReturn(listMahasiswaMock);
		mahasiswaController.deleteMahasiswa(nik);
		assertEquals(0, listMahasiswaMock.size());
	}
	
	@Test
	void testDeleteMahasiswaFail() {
		String nik = "NIK-124", name = "Akbar2", age = "20";
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();
		listMahasiswaMock.add(addMahasiswa(nik, name, age));
		
		lenient().when(mahasiswaInterfaceMock.retrieveAllData()).thenReturn(listMahasiswaMock);
		assertFalse(mahasiswaController.deleteMahasiswa("NIK-345"));
	}
	
	MahasiswaModel addMahasiswa(String nik, String name, String age) {
		MahasiswaModel newMahasiswa = new MahasiswaModel();
		newMahasiswa.setNik(nik);
		newMahasiswa.setName(name);
		newMahasiswa.setAge(age);
		return newMahasiswa;
	}

	@Test
	void testIsNikExist() {
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();
		listMahasiswaMock.add(addMahasiswa("NIK-123", "Akbar", "21"));
		assertEquals(0, mahasiswaController.isNikExist(listMahasiswaMock, "NIK-123"));
	}
	
	@Test
	void testIsNikExist_notFound() {
		ArrayList<MahasiswaModel> listMahasiswaMock = new ArrayList<MahasiswaModel>();
		listMahasiswaMock.add(addMahasiswa("NIK-123", "Akbar", "21"));
		assertEquals(-1, mahasiswaController.isNikExist(listMahasiswaMock, "NIK-12345"));
	}
	
	@Test
	void testIsValidNik_normal() {
		assertTrue(mahasiswaController.isValidNik("NIK-123"));
	}
	
	@Test
	void testIsValidNik_empty() {
		assertFalse(mahasiswaController.isValidNik("NIK-"));
	}
	
	@Test
	void testIsValidNik_alfa() {
		assertFalse(mahasiswaController.isValidNik("NIK-aaa"));
	}

	@Test
	void testIsValidName_alfa() {
		assertTrue(mahasiswaController.isValidName("akbar"));
	}
	
	@Test
	void testIsValidName_num() {
		assertFalse(mahasiswaController.isValidName("akbar123"));
	}

	@Test
	void testIsValidAge_normal() {
		assertTrue(mahasiswaController.isValidAge("21"));
	}
	
	@Test
	void testIsValidAge_under() {
		assertFalse(mahasiswaController.isValidAge("15"));
	}
	
	@Test
	void testIsValidAge_alfa() {
		assertFalse(mahasiswaController.isValidAge("aaa"));
	}
	
	@Test
	void testGetName() {
		String name = "Akbar";
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel.setName(name);
		assertEquals(name, mahasiswaModel.getName());
	}

	@Test
	void testGetAge() {
		String age = "20";
		MahasiswaModel mahasiswaModel = new MahasiswaModel();
		mahasiswaModel.setAge(age);
		assertEquals(age, mahasiswaModel.getAge());
	}

}
